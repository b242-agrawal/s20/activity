let number = parseInt(prompt("Enter a number"));

for(n=number; n>=0; n--){
	if(n<=50){
		break;
	}
	if(n%10 === 0){
		console.log("The number is divisible by 10. Skipping the number");
		continue;
	}
	if(n%5 === 0){
		console.log(n);
	}
}

let string = "supercalifragilisticexpialidocious";
let consonants = "";
for(let i = 0; i<string.length; i++){
	if(
		string[i] === 'a' ||
		string[i] === 'e' ||
		string[i] === 'i' ||
		string[i] === 'o' ||
		string[i] === 'u'
	){
			continue;
	}
	else {
		consonants	+= string[i];
	}
}
console.log(consonants);
